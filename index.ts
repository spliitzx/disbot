import { Client } from './classes/core/Client';
import { Login } from './classes/core/Login';
import { Logger } from './classes/Logger';
import { Init } from './classes/Init';
import { Route } from './classes/core/Route';
import { Event } from './classes/core/Event';
import { Message as MessageDirective } from './classes/core/Message';
import { Command as CommandDirective } from './classes/core/Command';
import { TaskScheduler as TaskSchedulerService } from './classes/core/TaskScheduler';

export var Disbot = {
    Client: Client,
    Login: Login,
    Init: (token: string) => {
        return new Init().init(token);
    },
    Route: Route,
    Event: (event: string, client: any) => {
        return new Event().emit(event, client);
    },
    Message: MessageDirective,
    Command: CommandDirective
}

export var Command = CommandDirective;
export var Message = MessageDirective;
export var TaskScheduler = TaskSchedulerService;
import * as Discord from 'discord.js';
import { Logger } from './Logger';
import { Login } from './core/Login';
import { Client } from './core/Client';

export class Init {
    public c: Discord.Client;
    constructor() {
        this.c = new Client().client
    }

    init(token: string) {
        return new Promise<any>((resolve, reject) => {
            new Login(this.c, token).login().then((data) => {
                if(typeof data !== "string")
                    reject('No token.');
                resolve({
                    token: data,
                    client: this.c
                });
            }).catch((e) => {
                reject(e);
            });
        });
    }
}
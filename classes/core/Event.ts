export class Event {
    constructor() {
    }

    emit(event: string, client: any) {
        return new Promise<any>((resolve, reject) => {
            client.on(event, (e: any) => {
                resolve(e);
            }).catch(() : void => {
                reject('Request failed.');
            });
        });
    }
}
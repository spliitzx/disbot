interface RouteOptions {
    name: string,
    component: any,
    data: any
}

export class Route {
    public message: any;
    public client: any;
    public args: any;

    constructor(private routeOptions: RouteOptions) {
        this.message = this.routeOptions.data;
        this.client = this.routeOptions.data.client;
        this.args = this.routeOptions.data.content.split(' ');

        this.route();
    }

    route() {
        new this.routeOptions.component({message: this.message, client: this.client, args: this.args});
    }
}
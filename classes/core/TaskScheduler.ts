import * as cron from 'node-cron';

interface Schedule {
    cron: string,
    component: any,
    options?: {
        timezone?: string,
        scheduled?: boolean
    }
};

export class TaskScheduler {
    constructor(private data: any, private options: Schedule) {
        this.run();
    }

    run() : void {
        let valid = cron.validate(this.options.cron);
        if(!valid)
            return console.warn('Invalid cron string provided.');
        
        cron.schedule(this.options.cron, () => {
            new this.options.component(this.data);
        }, this.options.options);
    }
}
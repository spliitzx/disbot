interface Params {
    client: any,
    message: any,
    args: any
};

export class Command {

    public client: any;
    public message: any;
    public args: any;

    constructor(params: Params) {
        this.client = params.client;
        this.message = params.message;
        this.args = params.args;

        this.run();
    }

    run() : void {

    }

    get isCommand() {
        return true;
    }

    sendEmbed(props: EmbedProperties) {
        return this.message.channel.send(props);
    }

    reply(msg: string) {
        if(msg.length > 2000)
            throw new Error('Failed to send message: character limit reached.');

        return this.message.reply(msg);
    }

    send(msg: string) {
        if(msg.length > 2000)
            throw new Error('Failed to send message: character limit reached.');

        return this.message.channel.send(msg);
    }

    concat(arg: number) {
        return this.args.slice(arg).join(' ');
    }
}

interface EmbedProperties {
    content?: string,
    embed: {
        title?: string,
        description?: string,
        url?: string,
        color?: number,
        timestamp?: string,
        footer?: {
            icon_url?: string,
            text: string
        },
        thumbnail?: {
            url: string
        },
        image?: {
            url: string
        },
        author?: {
            name: string,
            url?: string,
            icon_url?: string
        },
        fields?: any[]
    }
}
import * as Discord from 'discord.js';

export class Login {
    constructor(private client: Discord.Client, private token: string) {
        this.login();
    }

    login() {
        return this.client.login(this.token);
    }
}
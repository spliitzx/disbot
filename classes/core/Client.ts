import * as Discord from 'discord.js';

export class Client {
    public dClient: Discord.Client;

    constructor() {
        this.dClient = new Discord.Client();
    }

    get client() {
        return this.dClient;
    }
}
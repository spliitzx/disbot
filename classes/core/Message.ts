import { Route } from './Route';

interface Command {
    name: any;
    component: any;
    options?: Options;
};

interface Options {
    disabled?: boolean;
    adminOnly?: boolean;
    permissions?: any[];
}

export class Message {
    constructor(private data: any, private commands: Command[], private prefix: string) {
        this.run();
    }

    run() {
        if (this.data.author.bot) return;
        if (!this.data.content.startsWith(this.prefix)) return;
        let cmd = this.data.content.split(' ').shift().slice(this.prefix.length).toLowerCase();
        this.commands.map(async (e: any) => {

            let opt = await this.parseOptions(e);
            if (!opt) return false;

            let alias = [];

            if (e.name instanceof Array) {
                for (let i = 0; i < e.name.length; i++) {
                    const x = e.name[i].toLowerCase();
                    alias.push(x);
                }
            }

            if (typeof e.name == "string")
                alias.push(e.name);

            if (alias.indexOf(cmd) + 1) {
                new Route({
                    name: e.name,
                    component: e.component,
                    data: this.data
                });
            }
        });
    }

    async parseOptions(e: Command) {
        if (e.options && Object.keys(e.options).length !== 0) {
            if (e.options.disabled && e.options.disabled == true)
                return false;
            if (e.options.adminOnly && e.options.adminOnly == true)
                return this.data.member.hasPermission('ADMINISTRATOR');
            if (e.options.permissions && e.options.permissions.length > 0)
                return this.canExecute(e.options.permissions);
        } else {
            return true;
        }
    }

    canExecute(permissions: any[]) {
        if (this.data.member.permissions.has(permissions))
            return true;
        return false;
    }
}
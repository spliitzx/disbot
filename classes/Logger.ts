export class Logger {
    public message: string;
    public level: number;

    constructor(message: string, level: number) {
        this.message = message;
        this.level = level;
        this.log();
    }

    log() : void {
        console.log(this.message);
    }
}

/**
 * ## LEVELS ##
 * 1 - Fatal
 * 2 - Error
 * 3 - Warn
 * 4 - Info
 * 5 - Notify
 */
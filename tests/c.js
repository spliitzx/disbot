"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const __1 = require("..");
class C extends __1.Command {
    constructor(params) {
        super(params);
    }
    run() {
        let reason = this.concat(1);
        this.message.reply(reason);
    }
}
exports.C = C;

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const __1 = require("..");
const c_1 = require("./c");
const cron_1 = require("./cron");
var client = {};
__1.Disbot.Init('').then((data) => {
    console.log(data.client.user.tag);
    new __1.TaskScheduler(data, {
        cron: '*/1 * * * *',
        component: cron_1.Cron
    });
    data.client.on('message', (message) => {
        new __1.Message(message, [
            {
                name: [
                    'c',
                    'f',
                    'h'
                ],
                component: c_1.C,
                options: {
                    permissions: [
                        'ADMINISTRATOR'
                    ]
                }
            }
        ], ';');
    });
});

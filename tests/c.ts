import { Command } from '..';

export class C extends Command {

    constructor(params: any) {
        super(params);
    }

    run() {
        let reason = this.concat(1);
        this.message.reply(reason);
    }
}
import { Disbot, Message, TaskScheduler } from '..';
import { C } from './c';
import { Cron } from './cron';

var client: any = {};

Disbot.Init('').then((data: any) => {
    console.log(data.client.user.tag);

    new TaskScheduler(data, {
        cron: '*/1 * * * *',
        component: Cron
    });

    data.client.on('message', (message: any) => {
        new Message(message, [
            {
                name: [
                    'c',
                    'f',
                    'h'
                ],
                component: C,
                options: {
                    permissions: [
                        'ADMINISTRATOR'
                    ]
                }
            }
        ], ';');
    });
});
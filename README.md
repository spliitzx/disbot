# Disbot
Disbot is a package library written in TypeScript that interacts with the Discord API using Discord.js to make bots easier to create. It includes basic features from simple message event handling, to custom task scheduling using cron jobs.  
Disbot does all the validation for you, so there's no need to check for a prefix, or check if the author is a bot. It's all in the package!  

## Installation
Disbot can be used on JavaScript. For the TypeScript version, see URL  
`npm install --save disbot`
  
## Example Usage
index.js
```
import { Disbot } from 'disbot';
import { Ping } from './ping'
const Disbot = require('disbot').Disbot;
const Ping = require('ping');

Disbot.Init('bot_token').then((data) => {
    data.client.on('message', (message) => {
        new Message(message, [
            {
                name: 'ping',
                component: Ping
            }
        ], '!');
    });
});
```

## Example Command
Ping.js  
```
const Command = require('disbot').Command;

export class Ping extends Command {

    constructor(params) {
        super(params);
    }

    run() {
        // Command Logic
    }
}
```
The `params` property is passed by the `Command` class when the command is first initialized by the message service.  
`params` contains 3 properties and can be accessed in the child class:  
`this.message` - The message object.  
`this.client` - The bot client object.  
`this.args[]` - An array with all the commands arguments.  
  
### Built-In Command Methods
Disbot's command controller class contains several methods for making Discord bots as smooth as possible.  
`sendEmbed(props: Object)` - Sends an embedded message using the default Discord options.  
`reply(message: string)` - Replies to a message.  
`send(message: string)` - Sends a message to the current channel.  
`concat(arg: number)` - Concatenates a string (join) from the specified argument onwards.  
  
## Task Scheduler
index.js
```
new TaskScheduler(data, {
    cron: '*/1 * * * *',
    component: CronComponent'
});
```
  
Where `data` is the data returned on the `Disbot.Init()` function. Similar to the Command structure, TaskScheduler's `component` property takes a component file that is run when the cron is run, and repeated.
  
## Commands
index.js
```
data.client.on('message', (message) => {
    new Message(message, [
        {
            name: 'testcommand',
            component: TestCommand
        };
    ], prefix);
});
```
  
`data.client.on('message')` is fired whenever there is a new message in any channel.  
`prefix` - The command prefix. Leave the string empty if no prefix is required.  
The `name` property can be used in various ways:  
### String
Only allows 1 specific string to execute the command.
```
name: 'testcommand'
```
### Array
Allows multiple strings to execute the command.
```
name: [
    'testcommand',
    'testing',
    'imjusttesting'
]
```
You can pass options in each command object. The current only supported options are:
### Options (Optional)
```
options: {
    disabled: boolean,  // true or false; if true it stops the command
    adminOnly: boolean, // true or false; limits execution to administrators
    permissions: [] // the permissions required to execute the command
}
```